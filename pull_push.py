#!/bin/env python

import requests
import paramiko
from scp import SCPClient
import config as cfg
import hashlib


def main():
    session = requests.Session()
    session.headers.update({'User-Agent':
                            'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'})

    client = createSSHClient(cfg.to_host,
                             cfg.to_port,
                             cfg.to_username,
                             cfg.private_key,
                             cfg.password_for_key)
    scp = SCPClient(client.get_transport())

    for x in cfg.to_froms:
        try:
            response = session.get(x['from'])
        except Exception as error:
            print ('Request error: ', error)
            continue

        file_hash = hashlib.md5(x['from'].encode('utf-8')).hexdigest()
        tmp_file_path = '/tmp/push_pull_' + file_hash
        with open(tmp_file_path, "w") as f:
            f.write(response.text)
            try:
                scp.put(tmp_file_path, remote_path=x['to'])
            except Exception as error:
                print ('SCP error: ', error)
                continue
    scp.close()
    client.close()
    session.close()


def createSSHClient(hostname, port, myuser, key_file, password_for_key):
    _key = paramiko.Ed25519Key.from_private_key_file(key_file,
                                                     password_for_key)
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname, port, username=myuser, pkey=_key)
    return client


if __name__ == "__main__":
    main()
